// File Name GreetingClient.java
import java.net.*;
import java.util.ArrayList;
import java.io.*;
import org.w3c.dom.*;
import javax.xml.parsers.*;
import java.io.*;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;

public class GreetingClient {

	public static DataOutputStream out;
	public static Socket client;
	public static OutputStream outToServer;
	public static InputStream inFromServer;
	public static DataInputStream in;
	static ArrayList<Server> servers = new ArrayList<Server>();
	public static Server largestServer = new Server();

	public static void main(final String[] args) {

		final String serverName = args[0];
		final int port = Integer.parseInt(args[1]);

		try {

			System.out.println("Connecting to " + serverName + " on port " + port);

			initiliseSocketConnection(serverName, port);

			System.out.println("Just connected to " + client.getRemoteSocketAddress());

			// -------------------------------------//
			// ---- Define Some Local Variables ----//
			// -------------------------------------//
			String currentMsg = "";
			int okCnt = 0;
			int jobCnt = 0;

			// -------------------------------------//
			// -------------------------------------//

			sendMsg("HELO"); // Sends Initail Message To Server

			// -------------------------------------//
			// ------------- Main Loop -------------//
			// -------------------------------------//

			// Loop Executes Until "QUIT" Is Recived
			while (currentMsg != "QUIT") {

				currentMsg = readMsg();

				if (currentMsg.length() > 0) { // There is a message

					// ---------- Print The Message --------//
					System.out.print("RCVD: ");
					System.out.println(currentMsg);
					// -------------------------------------//

					if (currentMsg.equals("OK")) {
						okCnt = okCnt + 1; // Increment a counter for how many "ok's" have been received
						if (okCnt == 1) { // If the first ok
							sendMsg("AUTH GROUP15"); // AUTHENTICATE AS PER PROTOCOL
						} else if (okCnt == 2) { // IF the second ok

							loadSysXML();

							largestServer = getLargestServerFromSysXML(); // Workout which server type has the most
																			// cores

							System.out.println("-----------------");
							System.out.println("LARGEST SERVER IS: " + largestServer.type);
							System.out.println("-----------------");

							sendMsg("REDY"); // Tell Server The Client Is Ready For A Job
						} else if (okCnt >= 3) {
							// sendMsg("RESC All"); //Reqest Recource Information
							sendMsg("REDY");
						}
					} else if (currentMsg.contains("JOB")) { // If A Job Is Recived...

						System.out.println("RECIEVED A JOB!!!");

						sendMsg("SCHD " + jobCnt + " " + getBestServer()); // SCHEDULE THE JOB TO THE FIRST INSTANCE (0)
																			// OF THE LARGEST SERVER ^ CALCULATED
																			// PREVIOUSLY

						jobCnt = jobCnt + 1; // Incriment A Job Count Variable

					} else if (currentMsg.equals("NONE")) { // No More Jobs To Be Scheduled...

						System.out.println("NO More Jobs Left");

						sendMsg("QUIT"); // Safley shutdown server as per protocol

						// } else if (currentMsg.equals("DATA")) { //If DATA received

						// System.out.println("Data Received ...");

						// sendMsg("OK"); //Send OK to tell server you are ready to accept DATA

						// } else if (currentMsg.equals(".")) { //If "." received

						// System.out.println("RESC FINISHED");

						// sendMsg("REDY"); //Send ready to tell server the DATA was succesfuly received
						// and now ready to schedule jobs

					} else if (currentMsg.equals("QUIT")) { // If Quit Received, break the loop

						System.out.println("Recieved Quit... Quitting Now...");

						break;

					} else {
						sendMsg("OK"); // If the message does not meet any of the above criteria, it is sending
										// Resource Data, so respond with "OK"
					}

				}
				currentMsg = ""; // Reset Varaible
			}
			// -------------------------------------//
			// ---------- END MAIN LOOP ------------//
			// -------------------------------------//

			// -------------------------------------//
			// -------- SHUTDOWN THE CLIENT --------//
			// -- & SAFLEY STOP SOCKET CONNECTION---//
			// -------------------------------------//
			System.out.println("FINISHED");
			stopSocketConnection();
			// -------------------------------------//
			// -------------------------------------//

		} catch (final IOException e) {
			e.printStackTrace(); // CATCH IO EXCEPTIONS AND PRINT
		}

	}

	// GETS THE BEST SERVER - FOR FUTURE STAGES THIS WILL IMPLEMENT SOME FORM OF
	// CLEAVER ALGORITHIM...
	// FOR NOW JUST RETURNS ALL_TO_LARGE
	public static String getBestServer() {
		return largestServer.type + " 0";
	}

	// READS A MESSAGE FROM THE SERVER
	public static String readMsg() throws IOException {
		// final byte[] response = in.readNBytes(in.available()); // Read All Available bytes from socket input stream into
																// byte Array

			//final byte[] response = in.readNBytes(in.available()); // Read All Available bytes from socket input stream into

			byte[] targetArray = new byte[in.available()];
    		in.read(targetArray);


		return new String(targetArray).trim(); // Convert byte array to String and remove leading or trailing whitespaces
	}

	// SENDS A MESSAGE (STRING) TO THE SERVER
	public static void sendMsg(final String msg) throws IOException {
		System.out.println("SENDING: " + msg);
	
		final String s = msg;
		final byte[] message = s.getBytes();
		out.write(message);
		out.flush();
	
	}

	// SETS UP SOCKET CONNECTION WITH SERVER NAME & PORT FROM ARGS
	private static void initiliseSocketConnection(final String s, final int p) throws IOException {
		client = new Socket(s, p);
		outToServer = client.getOutputStream();
		out = new DataOutputStream(outToServer);
		inFromServer = client.getInputStream();
		in = new DataInputStream(inFromServer);
	}

	// SETS UP SOCKET CONNECTION WITH SERVER NAME & PORT FROM ARGS
	private static void stopSocketConnection() throws IOException {
		out.flush();
		out.close();
		in.close();
		client.close();
	}

	// Imports Server Data From System.xml into array list of Server Objects 'servers'
	public static void loadSysXML() {
		try {

			// Initiate Input Variables
			final File inputFile = new File("system.xml");
			final DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			final DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			final Document doc = dBuilder.parse(inputFile);
			doc.getDocumentElement().normalize();

			final NodeList nList = doc.getElementsByTagName("server"); // Reads xml where the tag is 'server ' into node
																		// list

			for (int temp = 0; temp < nList.getLength(); temp++) { // loop through node list
				final Node nNode = nList.item(temp);

				if (nNode.getNodeType() == Node.ELEMENT_NODE) { // If its an element
					final Element eElement = (Element) nNode;

					// ADD SERVER TO LIST WITH PROPERTIES FROM THE XML FILE
					servers.add(new Server(eElement.getAttribute(
							"type"),
					 eElement.getAttribute("limit"), 
					 eElement.getAttribute("bootupTime"),
					 eElement.getAttribute("hourlyRate"), 
					 eElement.getAttribute("memory"), 
					 eElement.getAttribute("disk"), 
					 Integer.parseInt((eElement.getAttribute("coreCount")))));
				 }
			  }	
			  
			  
		   } catch (final Exception e) {
			  e.printStackTrace(); //Catch errors
		   }	 
	}



   // LOOPS THROUGH ARRAY OF SERVER OBJECTS TO FIND SERVER WITH MOST CORES (IF MULTIPLE FOUND - RETURNS THE LAST FOUND SERVER)
   // RETURNS: Largest Server In Servers Array List
   public static Server getLargestServerFromSysXML() {
		
		if (servers.isEmpty()) {
			return new Server();
		}

		int maxCoreIndex = 0;
		for (int i=0; i<servers.size(); i++) //Loop through server list find largest core count
		{ 
			if (servers.get(i).coreCount > servers.get(maxCoreIndex).coreCount) {
				maxCoreIndex = i;
			}
		}

		return servers.get(maxCoreIndex); //return the largest server
   }
   


   
   
}

//Class for Server, simple properties
class Server {
	public String type;
	public String limit;
	public String bootupTime;
	public String hourlyRate;
	public Integer coreCount;
	public String memory;
	public String disk;

	public Server(final String t,final String l,final String b,final String h,final String m,final String d, final Integer c) { //Primary constructor
		type = t;
		limit = l;
		bootupTime = b;
		hourlyRate = h;
		coreCount = c;
		memory = m;
		disk = d;
	}

	public Server() { //Blank Constructor
		type = "";
		limit = "";
		bootupTime = "";
		hourlyRate = "";
		coreCount = 0;
		memory = "";
		disk = "";
	}


}